package top.cavecraft.spleefrun;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;

import static top.cavecraft.spleefrun.SpleefRun.*;
import static org.bukkit.Bukkit.*;

public class SpleefRunEvents implements Listener {
    List<Entity> chaosSnowballs = new ArrayList<>();

    @EventHandler
    public void dropItem(PlayerDropItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void playerDamage(EntityDamageEvent event) {
        if (started) {
            event.setDamage(0);
        } else {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void hunger(FoodLevelChangeEvent event) {
        event.setFoodLevel(20);
    }

    @EventHandler
    public void playerJoin(PlayerJoinEvent event) {
        if (getServer().getOnlinePlayers().size() > maxPlayers || started) {
            event.getPlayer().setGameMode(GameMode.SPECTATOR);
            event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 999999, 1, true));
            event.getPlayer().teleport(new Location(Bukkit.getWorlds().get(0), 0, 65, 0));
            event.setJoinMessage(ChatColor.GOLD + event.getPlayer().getDisplayName() + " is spectating.");
        }
    }

    @EventHandler
    public void inventoryClick(InventoryClickEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void snowballHit(ProjectileHitEvent event) {
        if (chaosSnowballs.contains(event.getEntity())) {
            chaosSnowballs.remove(event.getEntity());
            Location spawnLocation = event.getEntity().getLocation();
            spawnLocation.getWorld().createExplosion(spawnLocation, 2);
        }

        Block block = event.getEntity().getLocation().add(event.getEntity().getVelocity().normalize()).getBlock();
        block.setType(Material.AIR);
        block.getLocation().getWorld().playSound(block.getLocation(), Sound.DIG_SNOW, 8, 2);
    }

    @EventHandler
    public void mobSpawn(CreatureSpawnEvent event) {
        if (
                event.getEntity().getType() != EntityType.SNOWBALL &&
                event.getEntity().getType() != EntityType.PLAYER &&
                event.getEntity().getType() != EntityType.CREEPER
        ) {
            event.setCancelled(true);
        }
        if (event.getEntity().getType() == EntityType.CREEPER && event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.NATURAL) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void throwSnowball(ProjectileLaunchEvent event) {
        if (event.getEntity().getShooter() instanceof Player) {
            Player player = (Player)event.getEntity().getShooter();

            player.getInventory().setItem(0, new ItemStack(Material.SNOW_BALL, 64));
            player.getWorld().playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 0.5f, 0.1f);
        }
    }

    @EventHandler
    public void blockBreak(BlockBreakEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void blockPlace(BlockPlaceEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void playerInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (event.getPlayer().getInventory().getItemInHand().getType() == Material.TNT && started && event.getPlayer().getGameMode() != GameMode.SPECTATOR) {
            if (player.getInventory().getItemInHand().getAmount() > 1) {
                player.getInventory().getItemInHand().setAmount(player.getInventory().getItemInHand().getAmount() - 1);
            } else {
                player.getInventory().setItemInHand(new ItemStack(Material.AIR));
            }
            chaosSnowballs.add(player.launchProjectile(Snowball.class));
        }
        cclib.getGameUtils().blockInteract(event);
    }

    @EventHandler
    public void breakPainting(HangingBreakEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void playerPickupItem(PlayerPickupItemEvent event) {
        cclib.createActionBarMessage(ChatColor.RED + "You picked up a CHAOS SNOWBALL!").sendTo(event.getPlayer());
        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ORB_PICKUP, 1, 1);
        ItemStack chaosSnowball = new ItemStack(Material.TNT, 1);
        ItemMeta chaosMeta = chaosSnowball.getItemMeta();
        chaosMeta.setDisplayName(ChatColor.RESET + "Chaos Snowball");
        chaosSnowball.setItemMeta(chaosMeta);
        event.getPlayer().getInventory().addItem(chaosSnowball);
        event.getItem().remove();
        event.setCancelled(true);
    }
}
