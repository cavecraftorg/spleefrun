package top.cavecraft.spleefrun;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import top.cavecraft.cclib.ICCLib;
import top.cavecraft.cclib.ICCLib.IGameUtils.GemAmount;
import top.cavecraft.cclib.ICCLib.ITickReq;

import java.util.*;

public final class SpleefRun extends JavaPlugin {
    static boolean started = false;
    public static ICCLib cclib;
    static final Material[] snow = {
            Material.SNOW_BLOCK,
            Material.WOOL,
            Material.QUARTZ_BLOCK,
            Material.STAINED_GLASS,
            Material.GLASS
    };
    static final int maxPlayers = 8;
    static List<ITickReq> tickRequirements;

    boolean awardedWinner = false;
    int ticksPassed = 0;
    int playersLeft = maxPlayers;

    ArrayList<Block> groundToErode = new ArrayList<>();

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(new SpleefRunEvents(), this);
        getServer().getScheduler().runTaskTimer(this, this::gameTick, 0, 1);
        cclib = (ICCLib) getServer().getPluginManager().getPlugin("CCLib");

        tickRequirements = Arrays.asList(
                cclib.createTickReq(2, 50, true),
                cclib.createTickReq(3, 40, true),
                cclib.createTickReq(4, 30, true),
                cclib.createTickReq(5, 25, true),
                cclib.createTickReq(6, 20, true),
                cclib.createTickReq(7, 15, true),
                cclib.createTickReq(8, 10, true)
        );

        cclib.enableGameFeatures(tickRequirements, this::startGame, this::teleportToArena);
    }

    public void teleportToArena() {
        Collection<? extends Player> players = getServer().getOnlinePlayers();
        for (Player player : players) {
            player.getInventory().clear();
            player.teleport(
                    new Location(player.getWorld(), (Math.random() * 60) - 30, 65, (Math.random() * 60) - 30)
            );
        }
    }

    public void startGame() {
        started = true;
        Collection<? extends Player> players = getServer().getOnlinePlayers();
        playersLeft = players.size();
        for (Player player : players) {
            for (int i = 0; i < 4; i++) {
                double x = (Math.random() * 60) - 30;
                double y = (Math.random() * 55) + 45;
                double z = (Math.random() * 60) - 30;
                Location random = new Location(player.getWorld(), x, y, z);
                Creeper creeper = (Creeper) player.getWorld().spawnEntity(random, EntityType.CREEPER);
                creeper.setPowered(true);
            }
            player.getInventory().setItem(0, new ItemStack(Material.SNOW_BALL, 64));
            player.resetTitle();
        }
        getServer().getScheduler().runTaskTimer(this, this::erodeGround, 0, 20);
        getServer().getScheduler().runTaskTimer(this, this::awardCoins, 600, 600);
    }

    public void awardCoins() {
        Collection<? extends Player> players = getServer().getOnlinePlayers();
        for (Player player : players) {
            if (player.getGameMode() == GameMode.SURVIVAL) {
                if (playersLeft <= 4) {
                    cclib.getGameUtils().awardGems(player, GemAmount.MEDIUM);
                } else {
                    cclib.getGameUtils().awardGems(player, GemAmount.SMALL);
                }
            }
        }
    }

    public void gameTick() {
        if (started) {
            playerTick();
        }

        Collection<? extends Player> players = getServer().getOnlinePlayers();
        playersLeft = 0;
        for (Player player : players) {
            if (player.getGameMode() == GameMode.SURVIVAL) {
                playersLeft++;
            }
        }

        if (started && playersLeft <= 1 && !awardedWinner) {
            awardWinner();
        }
    }

    public void awardWinner() {
        awardedWinner = true;
        Player winner = null;
        Collection<? extends Player> players = getServer().getOnlinePlayers();
        for (Player player : players) {
            if (player.getGameMode() == GameMode.SURVIVAL) {
                cclib.getGameUtils().awardGems(player, GemAmount.EXTRA_LARGE);
                winner = player;
            }
        }
        if (winner == null) {
            getServer().broadcastMessage(ChatColor.WHITE + "The game was a tie!");
        } else {
            getServer().broadcastMessage(ChatColor.GREEN + winner.getDisplayName() + ChatColor.WHITE + " won the game!");
            cclib.getGameUtils().winnerEffect(winner);
        }
        getServer().getScheduler().runTaskLater(this, cclib::restartServer, 160);
    }

    public void playerTick() {
        if (!awardedWinner) {
            Collection<? extends Player> players = getServer().getOnlinePlayers();
            for (Player player : players) {
                if (player.getLocation().getY() <= 0 && player.getGameMode() != GameMode.SPECTATOR) {
                    player.teleport(new Location(player.getWorld(), 0, 65, 0));
                    player.setGameMode(GameMode.SPECTATOR);
                    getServer().broadcastMessage(ChatColor.GOLD + player.getDisplayName() + ChatColor.WHITE + " died.");
                    player.getWorld().playSound(player.getLocation(), Sound.BAT_DEATH, 8, 1);
                }
                if (player.getGameMode() == GameMode.SURVIVAL) {
                    for (int x = -1; x <= 1; x++) {
                        for (int y = -1; y <= 1; y++) {
                            Block block = cclib.offset(player.getLocation(), x, -0.3, y).getBlock();
                            if (!groundToErode.contains(block)) {
                                groundToErode.add(block);
                            }
                        }
                    }
                }
                player.getInventory().setItem(0, new ItemStack(Material.SNOW_BALL, 64));
            }
        }
    }

    public void erodeGround() {
        if (!awardedWinner) {
            ArrayList<Block> remove = new ArrayList<>();
            for (Block block : groundToErode) {
                if (block.getType() == Material.SNOW) {
                    block.setType(Material.AIR);
                    remove.add(block);
                } else {
                    boolean isSnow = false;
                    for (int i = 0; i < snow.length; i++) {
                        if (block.getType().equals(snow[i])) {
                            isSnow = true;
                            try {
                                block.setType(snow[i + 1]);
                            } catch (ArrayIndexOutOfBoundsException e) {
                                block.setType(Material.AIR);
                                remove.add(block);
                            }
                            break;
                        }
                    }

                    if (!isSnow && block.getType() != Material.AIR) {
                        block.setType(snow[0]);
                    }
                }
            }

            for (Block block : remove) {
                groundToErode.remove(block);
            }
        }
    }
}
